﻿using System;


namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Please enter 3 points!!!");

                Triangle triangle = new Triangle();

                if (IsTriangel(triangle))
                {
                    string typeTriangel = triangle.GetTypeTriangle();
                    Console.WriteLine($"Alpha={triangle.Alpha} Betta={triangle.Betta} Gamma={triangle.Gamma}");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"This is {typeTriangel} triangle");
                    Console.ResetColor();
                    break;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"With the given coordinates a triangle does not exist.\nPlease try again");
                    Console.ResetColor();
                    continue;
                }
            }
            Console.ReadLine();
        }
                
        static bool IsTriangel(Triangle triangle)
        {
            if (((triangle.A + triangle.B) > triangle.C) || ((triangle.A + triangle.C) > triangle.B) || ((triangle.B + triangle.C) > triangle.A))
                return true;
            else
                return false;          
        }
    }
}
