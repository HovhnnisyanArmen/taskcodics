﻿using System;
using System.Drawing;

namespace Task2
{
    public class Triangle
    {
        public Triangle()
        {
            GetPoints();
            GetSides();
            GetCorner();
        }

        public Point[] Point { get; private set; }

        // Sides of the triangle
        public double A { get; private set; }
        public double B { get; private set; }
        public double C { get; private set; }

        //Corner of the triangle
        public double Alpha { get; private set; }
        public double Betta { get; private set; }
        public double Gamma { get; private set; }

        public string GetTypeTriangle()
        {
            if (Alpha == 90 || Betta == 90 || Gamma == 90)
                return "a ritght";
            if (Alpha > 90 || Betta > 90 || Gamma > 90)
                return "an obtuse";
            else
                return "an acute";
        }

        private void  GetCorner()
        {
            Alpha = (((Math.Acos((B * B + C * C - A * A) / (2 * B * C)) * 180)) / Math.PI);
            Betta = (((Math.Acos((A * A + C * C - B * B) / (2 * A * C)) * 180)) / Math.PI);
            Gamma = (((Math.Acos((A * A + B * B - C * C) / (2 * A * B)) * 180)) / Math.PI);
        }

        private double GetSidePoints(Point point1, Point point2)
        {
            double side= Math.Sqrt(Math.Pow((point2.X - point1.X), 2) + Math.Pow((point2.Y - point1.Y), 2));
            return side;
        }

        private void GetSides()
        {
            A = GetSidePoints(Point[0], Point[2]);
            B = GetSidePoints(Point[1], Point[2]);
            C = GetSidePoints(Point[0], Point[1]);
        }

        private int TryReadPointXorY(string title)
        {
            int pointXorY;
            Console.Write(title+"=");
            while (!Int32.TryParse(Console.ReadLine(), out pointXorY))
            {
                Console.WriteLine($"This is not a point value\nPlease enter a point value");
                Console.Write(title + "=");
            }
            return pointXorY;
        }

        private Point TryReadPoint(string title)
        {
            Point point = new Point();
            point.X = TryReadPointXorY($"x{title}");
            point.Y = TryReadPointXorY($"y{title}");
            return point;
            
        }

        private void GetPoints()
        {
            Point = new Point[3];
            for (int i = 0; i < Point.Length; i++)
            {
                Point[i] = TryReadPoint($"{i + 1}");
            }
            
        }
    }
}
