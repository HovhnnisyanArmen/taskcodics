﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a natural number!!!");
            uint number;
            while (!uint.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine($"This is not a natural number\nPlease enter a natural number");
            }

            uint lessThan = 9;
            SumOfDigitsLessThanForTest(number, lessThan);
            //uint result = SumOfDigitsLessThan(number,lessThan);
            //Console.WriteLine(result);
            Console.ReadLine();

        }

        static uint SumOfDigitsLessThan(uint digit, uint lessThan)
        {
            uint sum = 0;
            while (digit != 0)
            {
                uint n = digit % 10;
                digit = digit / 10;
                sum += n;
            }
            if (sum > lessThan)
                SumOfDigitsLessThan(sum, lessThan);
            return sum;
        }

        static uint SumOfDigitsLessThanForTest(uint digit, uint lessThan)
        {
            Console.Write($"The sum of the digits of {digit} number is "); 
            uint sum = 0;
            while (digit != 0)
            {
                uint n = digit % 10;
                digit = digit / 10;
                sum += n;
            }
            Console.Write(sum);
            Console.WriteLine();
            if (sum > lessThan)
                SumOfDigitsLessThanForTest(sum, lessThan);
            return sum;
        }
    }
}
